import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { StyleguideModule } from './components/styleguide-component/styleguide.module';
import { LogService } from './services/log.service';
import { EwCommonModule } from '../common/common.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { CRUDExerciseComponent } from './crud-exercise/crud-exercise.component';
import { CRUDExerciseEditComponent } from './crud-exercise/crud-exercise-edit.component'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AngularMaterialModule } from './angular-material.module'
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    CRUDExerciseComponent,
    CRUDExerciseEditComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    EwCommonModule,
    StyleguideModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [
    LogService
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
}
