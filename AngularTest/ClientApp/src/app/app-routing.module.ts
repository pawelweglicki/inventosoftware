import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CRUDExerciseComponent } from './crud-exercise/crud-exercise.component';
import { CRUDExerciseEditComponent } from './crud-exercise/crud-exercise-edit.component';

const routes: Routes = [

    { path: '', component: CRUDExerciseComponent, pathMatch: 'full' },
    { path: 'product/:id', component: CRUDExerciseEditComponent},
    { path: 'product', component: CRUDExerciseEditComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }