import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators, AbstractControl, AsyncValidatorFn } from '@angular/forms';
import { Product } from './product';


@Component({
    selector: 'ew-crud-exercise-edit',
    templateUrl: './crud-exercise-edit.component.html',
    styleUrls: ['./crud-exercise-edit.component.css']
})
export class CRUDExerciseEditComponent implements OnInit {

    title: string;
    form: FormGroup;
    product: Product;
    id?: number;

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private http: HttpClient,
        @Inject('BASE_URL') private baseUrl: string) {
    }

    ngOnInit() {
        this.form = new FormGroup({
            name: new FormControl(''),
            description: new FormControl(''),
            amount: new FormControl(''),
            price: new FormControl('')
        });

        this.loadData();
    }

    loadData() {
        this.id = +this.activatedRoute.snapshot.paramMap.get('id');
        if (this.id) {
            var url = this.baseUrl + "api/Products/" + this.id;
            this.http.get<Product>(url).subscribe(result => {
                this.product = result;
                this.title = "Edit - " + this.product.name;
                this.form.patchValue(this.product);
            }, error => console.error(error));
        }
        else {
            this.title = "Create a new Product"
        }
    }

    onSubmit() {
        var product = (this.id) ? this.product : <Product>{};

        product.name = this.form.get("name").value;
        product.description = this.form.get("description").value;
        product.amount = this.form.get("amount").value;
        product.price = this.form.get("price").value;
        if (this.id) {
            var url = this.baseUrl + "api/Products/" + this.product.id;
            this.http
                .put<Product>(url, product)
                .subscribe(result => {
                    console.log("Product " + product.id + " has been updated.");
                    this.router.navigate(['/']);
                }, error => console.error(error));
        }
        else {
            var url = this.baseUrl + "api/Products";
            this.http
                .post<Product>(url, product)
                .subscribe(result => {
                    console.log("Product has been created.");
                    this.router.navigate(['/']);
                }, error => console.error(error));
        }
    }
    onDelete(){
        var product = this.product;

        var url = this.baseUrl + "api/Products/" + this.product.id;
            this.http
                .delete<Product>(url)
                .subscribe(result => {
                    console.log("Product " + product.id + " has been deleted.");
                    this.router.navigate(['/']);
                }, error => console.error(error));
    }
}