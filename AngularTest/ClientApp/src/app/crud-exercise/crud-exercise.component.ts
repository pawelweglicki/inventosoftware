import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Product } from './product';

@Component({
  selector: 'ew-crud-exercise',
  templateUrl: './crud-exercise.component.html',
  styleUrls: ['./crud-exercise.component.css']
})
export class CRUDExerciseComponent implements OnInit {
  public displayColumns: string[] = ['id', 'name', 'description', 'amount', 'price', 'time'];
  public products: MatTableDataSource<Product>;
  @ViewChild(MatSort) sort: MatSort;
  constructor(
    private http: HttpClient,
    @Inject('BASE_URL') private baseUrl: string) {

  }

  ngOnInit(): void {
    this.http.get<Product[]>(this.baseUrl + 'api/Products')
      .subscribe(result => {
        this.products = new MatTableDataSource<Product>(result);
        this.products.sort = this.sort;
      }, error => console.log(error));
  }

  applyFilter(filterValue: string){
    this.products.filter = filterValue;
  }

}
