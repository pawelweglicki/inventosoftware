export interface Product {
    id: number;
    name: string;
    description: string;
    amount: number;
    price: number;
    time: string;
}