export const environment = {
  production: true,
  debugLog: false, // if false - disables all console.logs
  aboutThisPath: '/_layouts/15/Applications/aboutthis/aboutthis.html'
};
