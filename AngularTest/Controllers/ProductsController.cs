using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using AngularTest.Data;
using AngularTest.Data.Models;
using System.Xml.Linq;
using System.IO;

namespace AngularTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IXmlDb _xmlDb;
        public ProductsController(IXmlDb xmlDb)
        {
            _xmlDb = xmlDb;
        }

        [HttpGet]
        public List<Product> Get()
        {
            var products = _xmlDb.GetSerializedProducts();
            return products;
        }

        [HttpGet("{id}")]
        public Product Get(int id)
        {
            var products = _xmlDb.GetSerializedProducts();
            return products.Find(p => p.Id == id);
        }

        [HttpPut("{id}")]
        public void Put(int id, Product product)
        {
            var document = _xmlDb.GetDbDocument();
            var element = document.Elements("ArrayOfProduct").Elements("Product").Where(p => p.Attribute("id").Value == id.ToString()).FirstOrDefault();
            var path = Path.Combine(Environment.CurrentDirectory, "Data/Source/database.xml");
            element.Attribute("name").Value = product.Name;
            element.Attribute("desc").Value = product.Description;
            element.Attribute("amount").Value = product.Amount.ToString();
            element.Attribute("price").Value = product.Price.ToString();
            element.Attribute("time").Value = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.ffff");
            document.Save(path);
        }

        [HttpPost]

        public void Post(Product product)
        {
            var document = _xmlDb.GetDbDocument();
            var uniqueId = document.Descendants("ArrayOfProduct").Elements("UniqueID").FirstOrDefault().Attribute("id");
            var path = Path.Combine(Environment.CurrentDirectory, "Data/Source/database.xml");
            document.Root.Add
            (
                new XElement
                    (
                        "Product",
                        new XAttribute("id", uniqueId.Value),
                        new XAttribute("name", $"{product.Name}"),
                        new XAttribute("desc", $"{product.Description}"),
                        new XAttribute("amount", $"{product.Amount}"),
                        new XAttribute("price", $"{product.Price}"),
                        new XAttribute("time", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.ffff"))
                    )
            );

            var id = Int32.Parse(uniqueId.Value);
            id++;
            uniqueId.SetValue(id.ToString());

            document.Save(path);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var document = _xmlDb.GetDbDocument();
            var path = Path.Combine(Environment.CurrentDirectory, "Data/Source/database.xml");
            document.Elements("ArrayOfProduct").Elements("Product").Where(p => p.Attribute("id").Value == id.ToString()).FirstOrDefault().Remove();
            document.Save(path);
        }
    }
}