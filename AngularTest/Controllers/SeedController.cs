using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AngularTest.Data.Models;
using System.IO;
using System.Xml.Serialization;
using System.Xml.Linq;

namespace AngularTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SeedController : ControllerBase
    {

        public SeedController()
        {
        }

        [HttpGet("{id}")]
        public string Get(int id)
        {
            var products = new List<Product>();
            var rng = new Random();
            var path = Path.Combine(Environment.CurrentDirectory, "Data/Source/database.xml");
            for (int i = 1; i <= id; i++)
            {
                products.Add(new Product
                {
                    Id = i,
                    Name = $"product{i}",
                    Description = $"This is the product{i}",
                    Amount = rng.Next(0, 100),
                    Price = Math.Round(rng.NextDouble(1, 10), 2),
                    Time = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.ffff")
                });
            }

            using (FileStream stream = System.IO.File.Create(path))
            {
                var xmlSerializer = new XmlSerializer(typeof(List<Product>));
                xmlSerializer.Serialize(stream, products);
            }

            XDocument document = XDocument.Load(path);
            document.Root.AddFirst
            (
                new XElement
                    (
                        "UniqueID", new XAttribute("id", $"{id + 1}")
                    )
            );
            document.Save(path);
            return "database has been created successfully.";
        }
    }
}