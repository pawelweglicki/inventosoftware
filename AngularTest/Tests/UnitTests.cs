using Microsoft.VisualStudio.TestTools.UnitTesting;
using AngularTest.Controllers;
using AngularTest.Data;
using System.Xml.Linq;
using System.Linq;
using System;

namespace Tests
{
    [TestClass]
    public class UnitTests
    {
        private readonly ProductsController _testContr; 
        private readonly XmlDb _xmlDb;
        private readonly Random _rng;

        public UnitTests()
        {
            _xmlDb = new XmlDb("../../../../Data/Source/database.xml");
            _testContr = new ProductsController(_xmlDb);
            _rng = new Random();
        }

        [TestMethod]
        public void TestGetMethod()
        {
            var controllerProducts = _testContr.Get();

            var path = "../../../../Data/Source/database.xml";
            var document = XDocument.Load(path);
            var dbProducts = document.Descendants("ArrayOfProduct").Elements("Product").ToList();

            Assert.AreEqual(controllerProducts.Count, dbProducts.Count);
        }

        [TestMethod]
        public void TestGetWithIDMethod()
        {
            var controllerProduct = _testContr.Get(9);

            int testId = 9;

            Assert.IsNotNull(controllerProduct);
            Assert.AreEqual(controllerProduct.Id, testId);
        }

        [TestMethod]
        public void TestRandomExtensionClass()
        {
            var randomNumber = _rng.NextDouble(1, 10);

            bool limit1 = false;
            bool limit2 = false;

            if(randomNumber > 1)
                limit1 = true;
            if(randomNumber < 10)
                limit2 = true;

            Assert.IsTrue(limit1);
            Assert.IsTrue(limit2);
        }
    }
}
