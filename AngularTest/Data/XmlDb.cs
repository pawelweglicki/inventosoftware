using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using System.Xml.Serialization;
using AngularTest.Data.Models;

namespace AngularTest.Data
{
    public class XmlDb : IXmlDb
    {
        private  string _path { get; set; }
        public XmlDb()
        {
            _path = Path.Combine(Environment.CurrentDirectory, "Data/Source/database.xml");
        }
        public XmlDb(string path)
        {
            _path = path;
        }
        public List<Product> GetSerializedProducts()
        {
            var xmlSerializer = new XmlSerializer(typeof(List<Product>)); 
            //var path = Path.Combine(Environment.CurrentDirectory, "Data/Source/database.xml");
            using FileStream xmlStream = File.Open(_path, FileMode.Open);
            var products = (List<Product>)xmlSerializer.Deserialize(xmlStream);
            return products;
        }

        public XDocument GetDbDocument()
        {
            //var path = Path.Combine(Environment.CurrentDirectory, "Data/Source/database.xml");
            var document = XDocument.Load(_path);
            return document;
        }
    }
    
}