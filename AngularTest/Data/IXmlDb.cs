using System.Collections.Generic;
using System.Xml.Linq;
using AngularTest.Data.Models;

namespace AngularTest.Data
{
    public interface IXmlDb
    {
        public List<Product> GetSerializedProducts();

        public XDocument GetDbDocument();
    }
}