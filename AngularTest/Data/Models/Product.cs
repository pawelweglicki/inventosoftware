using System;
using System.Xml.Serialization;

namespace AngularTest.Data.Models
{
    [Serializable]
    public class Product
    {
        public Product()
        {

        }

        [XmlAttribute("id")]
        public int Id { get; set; }
        [XmlAttribute("name")]
        public string Name { get; set; }
        [XmlAttribute("desc")]
        public string Description { get; set; }
        [XmlAttribute("amount")]
        public int Amount { get; set; }
        [XmlAttribute("price")]
        public double Price { get; set; }
        [XmlAttribute("time")]
        public string Time { get; set; }
    }   

}